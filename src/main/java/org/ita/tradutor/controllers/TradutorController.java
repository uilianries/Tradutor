package org.ita.tradutor.controllers;


import org.ita.tradutor.models.Dicionario;
import org.ita.tradutor.models.DicionarioImpl;  

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller para tradução
 */
@WebServlet("/traduzir")
public class TradutorController extends HttpServlet {
    private Dicionario dicionario = new DicionarioImpl();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String palavra = request.getParameter("input-palavra");
        String traducao = dicionario.traduzir(palavra);
        request.setAttribute("traducao", traducao);
        request.getRequestDispatcher("resposta.jsp").forward(request, response);
    }
}
