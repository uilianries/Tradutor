package org.ita.tradutor.models;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * Carrega base de dados do arquivo JSON
 */
public class DicionarioImpl implements Dicionario {
    private BiMap<String, String> palavras = HashBiMap.create();

    public DicionarioImpl() {
        JSONObject json = carregarBaseDados();
        for (Object object : json.keySet()) {
            String key = (String) object;
            String mapped = (String) json.get(key);
            palavras.put(key, mapped);
        }
    }

    private JSONObject carregarBaseDados() {
        JSONObject jsonObject = null;
        JSONParser jsonParser = new JSONParser();
        try {
            Object object = jsonParser.parse(new FileReader("/tmp/dicionario.json"));
            jsonObject = (JSONObject) object;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public String traduzir(String palavra) {
        String traducao = palavras.get(palavra.toLowerCase());
        if (traducao != null && !traducao.isEmpty()) {
            return traducao;
        }
        traducao = palavras.inverse().get(palavra.toLowerCase());
        if (traducao != null && !traducao.isEmpty()) {
            return traducao;
        }
        return palavra;
    }
}
