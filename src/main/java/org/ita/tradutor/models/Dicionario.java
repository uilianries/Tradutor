package org.ita.tradutor.models;


/**
 * Carrega base de dados
 */
public interface Dicionario {
    /**
     * Traduz uma palavra de um idioma para outro
     * @param palavra Palavra deverá ser traduzida
     * @return Significado da plavra, no segundo idioma
     */
    public String traduzir(final String palavra);
}
