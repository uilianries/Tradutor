package org.ita.tradutor.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Valida comportamento do dicionario
 */
public class DicionarioTest {
    private Dicionario dicionario;

    @Before
    public void setUp() {
        this.dicionario = new DicionarioImpl();
    }

    /**
     * Traduz palavras para:
     * - Português -> Inglês
     * - Inglês -> Português
     * - Português -> Português
     * - Inglês -> Inglês
     */
    @Test
    public void traduzir() {
        assertEquals("house", this.dicionario.traduzir("casa"));
        assertEquals("motocicleta", this.dicionario.traduzir("motorcycle"));
        assertEquals("sapato", this.dicionario.traduzir("sapato"));
        assertEquals("wallet", this.dicionario.traduzir("wallet"));
    }

}