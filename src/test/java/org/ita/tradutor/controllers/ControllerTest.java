package org.ita.tradutor.controllers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ControllerTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8080/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testPt2En() throws Exception {
        driver.get(baseUrl + "/Tradutor_war_exploded/");
        driver.findElement(By.name("input-palavra")).clear();
        driver.findElement(By.name("input-palavra")).sendKeys("casa");
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
        assertEquals("Tradução: house", driver.findElement(By.id("resultado")).getText());
    }

    @Test
    public void testEn2Pt() throws Exception {
        driver.get(baseUrl + "/Tradutor_war_exploded/");
        driver.findElement(By.name("input-palavra")).clear();
        driver.findElement(By.name("input-palavra")).sendKeys("motorcycle");
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
        assertEquals("Tradução: motocicleta", driver.findElement(By.id("resultado")).getText());
    }

    @Test
    public void testPt2Pt() throws Exception {
        driver.get(baseUrl + "/Tradutor_war_exploded/");
        driver.findElement(By.name("input-palavra")).clear();
        driver.findElement(By.name("input-palavra")).sendKeys("furadeira");
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
        assertEquals("Tradução: furadeira", driver.findElement(By.id("resultado")).getText());
    }

    @Test
    public void testEn2En() throws Exception {
        driver.get(baseUrl + "/Tradutor_war_exploded/");
        driver.findElement(By.name("input-palavra")).clear();
        driver.findElement(By.name("input-palavra")).sendKeys("hurricane");
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
        assertEquals("Tradução: hurricane", driver.findElement(By.id("resultado")).getText());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
